"""
example:

 python -m derived_vars.calc_theta

"""
from thermo.thermlib import find_theta
from netCDF4 import Dataset
import glob
import numpy as np
import argparse

if __name__ == "__main__":
    
    linebreaks=argparse.RawTextHelpFormatter
    descrip=__doc__.lstrip()
    parser = argparse.ArgumentParser(formatter_class=linebreaks,description=descrip)
    args= parser.parse_args()
    
    var_dir = '/pip_raid/mrautenh/BOMEX/variables'
    var_files = glob.glob('{}/*nc'.format(var_dir))
    out_name = 'theta.nc'

    with Dataset(var_files[0],'r') as var_nc, Dataset(out_name,'w') as out_nc:
        temp = var_nc.variables['TABS']
        in_dims = var_nc.dimensions
        temp_dims=temp.dimensions
        print(temp_dims)
        for key,value in in_dims.items():
            if key in ['z','y','x']:
                inDim=len(value)
                out_nc.createDimension(key,inDim)
        dim_list = ['z','y','x']
        temp_vals = temp[...].squeeze()
        press_vals = var_nc.variables['p'][...]*100.
        press_vals = press_vals[:,np.newaxis,np.newaxis]
        theta_field = find_theta(temp_vals,press_vals)
        theta_nc = out_nc.createVariable('theta',theta_field.dtype,dim_list)
        theta_nc[...] = theta_field[...]

